const mongoose = require('mongoose');

const standSchema = mongoose.Schema({
    shortName: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    size: {
        type: String,
        require: true
    },
    x: {
        type: Number,
        require: true
    },
    y: {
        type: Number,
        require: true
    },
    informations: {
        type: String,
        require: true,
    },
    ownerId: {
        type: String,
        required: true
    }
}, { timestamps: true });

const Stand = mongoose.model('Stand', standSchema);

module.exports = { Stand }