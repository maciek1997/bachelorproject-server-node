
const express = require("express");
const bodyParse = require("body-parser");
const cookieParper = require("cookie-parser");
const mongoose = require("mongoose");
const config = require("./config/config").get(process.env.NODE_ENV);
const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(config.DATABASE);

const { User } = require('./models/user');
const { Stand } = require('./models/stand');
const { auth } = require('./middleware/auth')

app.use(bodyParse.json());
app.use(cookieParper());

app.use(express.static('client/build'))

// GET //

app.get('/api/auth', auth, (req, res) => {
    res.json({
        isAuth: true,
        id: req.user._id,
        email: req.user.email,
        name: req.user.name,
        lastname: req.user.lastname
    })
});


app.get('/api/logout', auth, (req, res) => {
    req.user.deleteToken(req.token, (err, user) => {
        if (err) return res.status(400).send(err);
        res.sendStatus(200)
    })
})


app.get('/api/getStand', (req, res) => {
    let id = req.query.id;

    Stand.findById(id, (err, doc) => {
        if (err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/api/allStands', (req, res) => {
    // localhost:3001/api/allStands?skip=3&limit=2&order=asc
    let skip = parseInt(req.query.skip);
    let limit = parseInt(req.query.limit);
    let order = req.query.order;
    let prefix = req.query.prefix;

    Stand.find().skip(skip).sort({ _id: order }).limit(limit).exec((err, doc) => {
        if (err) return res.status(400).send(err);
        res.send(doc);
    })
})



// POST //
app.post('/api/stand', (req, res) => {
    const stand = new Stand(req.body)

    stand.save((err, doc) => {
        if (err) return res.status(400).send(err);
        res.status(200).json({
            post: true,
            standId: doc._id
        })
    })
})

app.get('/api/user_stands', (req, res) => {
    Stand.find({ ownerId: req.query.user }).exec((err, docs) => {
        if (err) return res.status(400).send(err);
        res.send(docs)
    })
})

app.post('/api/register', (req, res) => {
    const user = new User(req.body);

    user.save((err, doc) => {
        if (err) return res.json({ success: false });
        res.status(200).json({
            success: true,
            user: doc
        })
    })
})

app.post('/api/login', (req, res) => {
    User.findOne({ 'email': req.body.email }, (err, user) => {
        if (!user) return res.json({ isAuth: false, message: 'Auth failed, email not found' })

        user.comparePassword(req.body.password, (err, isMatch) => {
            if (!isMatch) return res.json({
                isAuth: false,
                message: 'Wrong password'
            });

            user.generateToken((err, user) => {
                if (err) return res.status(400).send(err);
                res.cookie('auth', user.token).json({
                    isAuth: true,
                    id: user._id,
                    email: user.email
                })
            })
        })
    })
})


// UPDATE //

app.post('/api/stand_update', (req, res) => {
    Stand.findByIdAndUpdate(req.body._id, req.body, { new: true }, (err, doc) => {
        if (err) return res.statusMessage(400).send(err);
        res.json({
            success: true,
            doc
        })
    })
})

// DELETE //

app.delete('/api/delete_stand', (req, res) => {
    let id = req.query.id;

    Stand.findByIdAndRemove(id, (err, doc) => {
        if (err) return res.statusMessage(400).send(err);
        res.json(true);
    })
})



if (process.env.NODE_ENV === 'production') {
    const path = require('path');
    app.get('/*', (req, res) => {
        res.sendfile(path.resolve(__dirname, '../client', 'build', 'index.html'))
    })
}


const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log("listen at 3001");
});

