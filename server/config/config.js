const config = {
    production: {
        DATABASE: process.env.MONGODB_URL,
        SECRET: "password123"
    },
    default: {
        DATABASE: 'mongodb://localhost:27017/standVisualisation',
        SECRET: "password123"
    }
}

exports.get = function get(env) {
    return config[env] || config.default
}